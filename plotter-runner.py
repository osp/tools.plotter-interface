#!/usr/bin/env python2
# -*- coding: utf-8 -*-


import chiplotle3 as chiplotle
import os.path
import time
import glob
from settings import HPGL_CACHEDIR, HPGL_PLOTTEDDIR
from shutil import move
from settings import USE_VIRTUAL_PLOTTER
from os import makedirs
import codecs

if not os.path.exists(HPGL_CACHEDIR):
  makedirs(HPGL_CACHEDIR)

if not os.path.exists(HPGL_PLOTTEDDIR):
  makedirs(HPGL_PLOTTEDDIR)

def get_oldest_cachefile():
  files = [path for path in glob.glob(
    os.path.join(HPGL_CACHEDIR, '*.hpgl'))]
  if files:
    return sorted(files, key=lambda p, _: int(os.path.getmtime(p)))[0]
  else:
      return None

while True:
  if USE_VIRTUAL_PLOTTER:
    plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter()
  else:
    plotters = chiplotle.tools.plottertools.instantiate_plotters()
    if plotters:
      plotter = plotters[0]
    else:
      plotter = None

  # Not able to instantiate a plotter now. Trying again in 30 seconds.
  if not plotter:
    time.sleep(30)
    continue

  while True:
    cachefile = get_oldest_cachefile()
    if cachefile:
      print(cachefile)
      with codecs.open(cachefile, 'r', encoding='utf-8') as h:
        hpgl = h.read()
        # try:
        # plotter.write(u'IN;{};PG;'.format(hpgl).decode("utf-8").encode('ascii'))
        plotter.write('VS5;')
        plotter.write(hpgl.decode("utf-8").encode('ascii'))

        move(cachefile, os.path.join(HPGL_CACHEDIR, 'plotted', os.path.basename(cachefile)))

        if USE_VIRTUAL_PLOTTER:
          chiplotle.tools.io.view(plotter)
          plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter()
    time.sleep(1)
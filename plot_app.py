from flask import Flask, request, stream_with_context
from settings import BASEURL, HPGL_PLOT_PREFIX
from chiplotle3.tools.plottertools._instantiate_plotter import _instantiate_plotter
from chiplotle3.tools.serialtools.sniff_ports_for_plotters import sniff_ports_for_plotters
from chiplotle3.tools.serialtools.scan_serial_ports import scan_serial_ports

app = Flask(__name__)

@app.route("{}/plot".format(BASEURL), methods=["POST"])
def plot():
  def write_hpgl():
    hpgl = ';'.join([part.strip() for part in request.form['hpgl'].strip().split(';')])
    try:
      # From Chiplotle sources

      '''Dynamically searches and instantiates all found plotters.
      The function sniffs all serial ports in search for pen plotters and
      instantiates all plotters found. If a plotter is not recognized,
      the function interactively queries user for plotter type.'''
      yield '<html><head><title>Plotting.</title><style>html,body {font-family: monospace;}</style><body>'
      yield 'Scanning serial ports...<br />'
      ports = list(scan_serial_ports( ).values( ))
      yield 'Found ports:<br />'
      yield '&nbsp;&nbsp;' + '<br />&nbsp;&nbsp;'.join(ports) + '<br />'

      ## get serial ports that have a plotter connected...
      yield 'Sniffing for plotters in all serial ports...<br />'
      plotters_found = sniff_ports_for_plotters(ports)
      if len(plotters_found) == 0:
        yield 'Found no plotter connected to any of the serial ports.<br />'
        yield 'Is your plotter on?<br />'
        ## return a list so we don't get a python error when trying 
        ## to index the result.
        
      else:
        for serial_address, pln in list(plotters_found.items( )):
            yield '   Found plotter %s in port %s<br />' % (pln, serial_address)

        serial_address, pln = list(plotters_found.items())[0]
        plotter = _instantiate_plotter(serial_address, pln)

        yield "Sending HPGL...<br/>"
        plotter.write(HPGL_PLOT_PREFIX)
        plotter.write(hpgl)
        yield "All HPGL sent."
    except Exception as e:
      yield "Unknown error during plotting: <br/>{}.".format(str(e))
    finally:
      yield "</body></html>"

  return stream_with_context(write_hpgl())

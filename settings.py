# -*- coding: utf-8 -*-
import os.path

DEBUG = True
USE_VIRTUAL_PLOTTER = False
BASEPATH = os.path.dirname(os.path.realpath(__file__))

HPGL_CACHEDIR = os.path.join(BASEPATH, 'plots')
HPGL_PLOTTEDDIR =  os.path.join(HPGL_CACHEDIR, 'plotted')
HPGL_PREVIEWDIR = os.path.join(BASEPATH, 'preview')

# Initialization code to be sent to the plotter.
# To reset scaling a set a pen speed.
HPGL_PLOT_PREFIX = 'IN;VS15;'

BASEURL = ''
PLOT_SERVICE_URL = 'http://localhost:5001/plot'

# left -16240;
# bottom -11637;
# right 15280;
# top 11637;

#PAGE = {
#  'top': 11637,
#  'right': 15280,
#  'left': -16240,
#  'bottom': -11637
#}

PAGE = {
  'top': 11880,
  'right': 16800,
  'left': 0,
  'bottom': 0
}

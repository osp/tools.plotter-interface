from flask import Flask, jsonify, request, send_file
from export import export
from tempfile import mkstemp
from settings import BASEURL, HPGL_CACHEDIR, HPGL_PREVIEWDIR, PAGE
import os.path
from os import makedirs
from hashlib import blake2b

if not os.path.exists(HPGL_CACHEDIR):
  makedirs(HPGL_CACHEDIR)

if not os.path.exists(HPGL_PREVIEWDIR):
  makedirs(HPGL_PREVIEWDIR)

app = Flask(__name__)

@app.after_request
def add_headers(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers',
                        'Content-Type,Authorization')

  return response

@app.route("{}/plot".format(BASEURL), methods=["POST"])
def plot():
  hpgl = ';'.join([part.strip() for part in request.form['hpgl'].strip().split(';')])
  _, path = mkstemp(suffix='.hpgl', text=True, dir=HPGL_CACHEDIR)

  with open(path, 'w') as h:
    h.write(hpgl)

  h.close()
  return '100'

@app.route("{}/preview".format(BASEURL), methods=["POST"])
def preview():
  hpgl = ''
  frame = int(request.form['frame'])

  if frame > 0:
    hpgl += 'SP1;PA{left},{bottom};EA{right},{top};SP0;'.format(left=PAGE['left'], bottom=PAGE['bottom'], right=PAGE['right'], top=PAGE['top'])

  hpgl += ';'.join([part.strip() for part in request.form['hpgl'].strip().split(';')])

  # Hash hpgl code to derive a preview filename to avoid generating code again.
  h = blake2b(digest_size=32)
  h.update(hpgl.encode('ascii'))
  hpgl_hash = h.hexdigest()
  basepath = os.path.join(HPGL_PREVIEWDIR, '{}'.format(hpgl_hash))

  if not os.path.exists('{}.svg'.format(basepath)):
    export(hpgl, basepath, 'svg')

  # _, path = mkstemp(suffix='.hpgl', text=True, dir=HPGL_PREVIEWDIR)

  return jsonify({'path': '{}/plot-preview/{}.svg'.format(BASEURL, os.path.basename(basepath))})

if app.config['DEBUG']:
  @app.route("{}/".format(BASEURL))
  def index():
    return send_file('interface/index.html')

  @app.route("{}/code-hpgl.html".format(BASEURL))
  def send_code_page ():
    return send_file('interface/code-hpgl.html')

import subprocess
import os.path
# from settings import PAGE


# From chiplotle
def export(hpgl, filename, fmt = 'eps'):
  '''Export Chiplotle-HPGL objects to an image file format via ``hp2xx``.

  - `expr` can be an iterable (e.g., list) of Chiplotle-HPGL objects or a
    single Chiplotle-HPGL object.
  - `filename` the file name, including path but without extension.
  - `fmt` is a string describing the format of the file to which the 
    Chiplotle-HPGL objects will be exported. Default is 'eps'. 
    Valid formats are: jpg, png, tiff and many others. 
    Please see the ``hp2xx`` documentation for details.

  .. note::
    You must have ``hp2xx`` installed before you can export Chiplote-HPGL
    objects to image files.
  '''

  dirname = os.path.dirname(filename)
  hpglfile = '{0}.hpgl'.format(os.path.basename(filename))
  imgfile  = '{0}.{1}'.format(os.path.basename(filename), fmt)


  with open(os.path.join(dirname, hpglfile), 'w') as o:
    o.write(hpgl)

  cmd = 'cd {} && hp2xx --truesize -p 1 -m {} -f "{}" "{}"'.format(dirname, fmt, imgfile, hpglfile)
  p = subprocess.Popen(cmd, 
                      shell  = True,
                      stdout = subprocess.PIPE, 
                      stderr = subprocess.PIPE)
  stdout, stderr = p.communicate()
  # print(cmd)
  # print(stdout, stderr)

  return imgfile